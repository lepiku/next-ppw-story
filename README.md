# PPW Story #6 and up

[![Pipeline](https://gitlab.com/khadijah.rizqy/next-ppw-story/badges/master/pipeline.svg)](https://gitlab.com/khadijah.rizqy/next-ppw-story/pipelines)
![Coverage](https://gitlab.com/khadijah.rizqy/next-ppw-story/badges/master/coverage.svg)

This is the repository for my ppw stories from #6 Story up.

**#6 PEWE's Story: Status Website**

For the #6 PEWE's Story, I made a new status website using TDD. The website will contain form for accepting status responses. The form only accepts status with maximum length 300 characters. Once submitted, it will be displayed with automated created date-time.
