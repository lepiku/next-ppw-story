from django.shortcuts import redirect, render
from .forms import StatusForm
from .models import Status

# Create your views here.

def add_status(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('story_6:add_status')
        
    else:
        form = StatusForm()

    content = {
        'form' : form,
        'all_status' : Status.objects.all().order_by('-id')
    }

    return render(request, 'status.html', content)
